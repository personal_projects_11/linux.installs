# Install Ubuntu/Mint/Debian

## Snap 

```
sudo rm /etc/apt/preferences.d/nosnap.pref
sudo apt install snapd
sudo apt update
sudo apt install snapd
sudo apt update
```


```
sudo snap install slack --classic
sudo snap install --classic code # or code-insiders
sudo snap install dbeaver-ce
sudo snap install slack
sudo snap install postman
sudo snap install google-chat-electron
```

### Configuracion dbeaver
authenticationSchema => ntlm
integratedSecurity => true
custom properties: 
    user -> lbertolini
    password -> ****

## apt install

```
sudo apt-get install meld
sudo apt-get install virtualbox
sudo apt-get install git
sudo apt-get install gparted
```

### Guardar contraseÃ±as git

Si queremos con timeout -> git config --global credential.helper 'cache --timeout 3600'
Sin timeout -> git config --global credential.helper store



### brave

```
sudo apt install apt-transport-https curl
sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main"|sudo tee /etc/apt/sources.list.d/brave-browser-release.list
sudo apt update
sudo apt install brave-browser
```

### chat - Via chrome
```
sudo apt-get install chromium
sudo apt-get install git
sudo apt-get install notepadqq
sudo apt-get install thunderbird
```

### oh my bash
Ref: https://github.com/ohmybash/oh-my-bash

```
bash -c "$(wget https://raw.githubusercontent.com/ohmybash/oh-my-bash/master/tools/install.sh -O -)"
```

Configurar theme: 
https://github.com/ohmybash/oh-my-bash - Config plugin. Recomendacion: 

Sugerencias:
* ! brainy
*   sexy
*   hawaii50 - Da la ip en el mismo prompt
*   luan 
*   agnoster

### Sublime
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
sudo apt-get install apt-transport-https
sudo apt-get update
sudo apt-get install sublime-text


## python

```
sudo apt install python3 python3-pip python3-venv
sudo apt install jupyter-notebook
/bin/python3 -m pip install -U black 
pip install nb_black
```

## SQL server

```
sudo su
curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
curl https://packages.microsoft.com/config/debian/10/prod.list > /etc/apt/sources.list.d/mssql-release.list
exit

sudo apt-get update
sudo ACCEPT_EULA=Y apt-get install -y msodbcsql17

# optional: for bcp and sqlcmd
sudo ACCEPT_EULA=Y apt-get install -y mssql-tools
echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bashrc
source ~/.bashrc

# optional: for unixODBC development headers
sudo apt-get install -y unixodbc-dev

# optional: kerberos library for debian-slim distributions
sudo apt-get install -y libgssapi-krb5-2
sudo apt update && sudo apt upgrade
sudo wget -qO- https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -
sudo add-apt-repository "$(wget -qO- https://packages.microsoft.com/config/ubuntu/18.04/mssql-server-2019.list)"
sudo apt update

sudo apt install mssql-server
sudo /opt/mssql/bin/mssql-conf setup
curl https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add –
curl https://packages.microsoft.com/config/ubuntu/19.10/prod.list | sudo tee /etc/apt/sources.list.d/msprod.list
sudo apt update
sudo apt install mssql-tools
echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bash_profile 
echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bashrc 
source ~/.bashrc
```

Test conexión: 

* En consola: 

```
"/opt/mssql-tools/bin/sqlcmd -b -S SRVDB04-int -U sql_airflow-int -P brcpxWRH3UavydPB3sNh -d DWH_DS"
```

Fix del tema SSL - si tira __unable to connect__: https://askubuntu.com/questions/1233186/ubuntu-20-04-how-to-set-lower-ssl-security-level


## Docker

**Ref**: https://techviewleo.com/how-to-install-and-use-docker-in-linux-mint/

```
sudo apt-get update
sudo apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(. /etc/os-release; echo "$UBUNTU_CODENAME") stable"
sudo apt-get update
sudo apt-get -y install docker-ce
sudo usermod -aG docker $USER
newgrp docker
sudo docker run hello-world
```

### Portainer

```
sudo docker volume create portainer_data
docker run -d -p 8000:8000 -p 9000:9000 --name=portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce
```

TODO:
* Build airflow
* Compose airflow


### Hadolint

Ref: https://rebanking.atlassian.net/wiki/spaces/ARQ/pages/2099740673/hadolint+-+Haskell+Dockerfile+Linter

```
curl -sSL https://get.haskellstack.org/ | sh
cd hadolint
stack install
export PATH="$HOME/.local/bin:$PATH"
```

## Kubernetes

```
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
sudo install minikube-linux-amd64 /usr/local/bin/minikube
minikube start --nodes 3
minikube addons enable dashboard
minikube addons enable ingress
minikube addons enable registry
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
```

Check:

```
kubectl cluster-info
```

## Vs code

Plugins

* Docker
* Hadolint
* Jupyter
* Pylance
* Python docstring
* Markdown Preview Enhanced













